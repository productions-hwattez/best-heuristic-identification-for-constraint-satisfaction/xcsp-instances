# XCSP instances

This repository contains all XCSP instances (CSP and COP) from the last competitions. A subset is used for the current article and is specified within the campaigns (in the 'input_set' folder).

This repository has two branches:

- a branch composed only of [CSP instances](/../../../-/tree/csp)
- a branch composed only of [COP instances](/../../../-/tree/cop)

The campaigns are described in more detail in this [repository](/../../../../experiments/).
